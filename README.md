# React

- [VS 2017 React SPA template](https://docs.microsoft.com/en-us/aspnet/core/spa/react?tabs=visual-studio)
- [React+Redux+TypeScript `connect` decorator problem](https://bloggo.herokuapp.com/post/react-redux-typescript-connect-decorator-demo)

- Check out [React in Patterns](https://github.com/krasimir/react-in-patterns)
- Check out [You Probably Don't Need Derived State](https://reactjs.org/blog/2018/06/07/you-probably-dont-need-derived-state.html)

## Refs

Refs are useful for when you need to do imperative stuff over a React component (these are declarative).

Imagine you are building a React autocomplete textbox component. The idea is fairly simple: render a container with an `input` in it and a `select` underneath. After each keystroke in the `input`, fire off an AJAX request or something and when it comes back, update the `select` with new items. Of course it is more involved than that:
- requests may come in in incorrect order and you need to tag them and correlate responses with the current request to display the right choices
- you may want to make sure you only fire one request at a time (as an alternative to tagging requests and correlating them to fix the above)
- you generally want to fire off a request only after ceasing to type for one second or something

Once you click an option in the `select` you want the `input` to reflect it and move the focus back on it, because the `select` will capture it after the click.

This is where `refs` come in.

In prior versions of React `refs` were strings you tagged your elements with and you had the `React.findDOMNode` API to look up a DOM node based on the ref name.

This is no longer the case and `refs` are now functions which fire with an instance of the DOM node once the DOM node mounts and with `null` when it unmounts.

Here's how you can take advantage of that:

```js
class AutocompleteTextBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = { value: '', suggestions: [] };
    }

    onValueRef = node => {
        this.inputNode = node;
    }

    onValueChange = event => {
        const value = event.target.value;
        this.setState({ suggestions: [ value + "a", value + "b", value + "c" ], value });
    }

    onSuggestionSelect => event => {
        const suggestion = event.target.value;
        this.setState({ value: suggestion });
        // This will be undefined before the `input` mounts and null once the `input` unmounts.
        // That shouldn't be a problem though because users have no way of clicking `option`s that are not mounted yet or no more.
        this.inputNode.focus(); // Move focus back to the `input` after it has lost it when an `option` was clicked!
        this.inputNode.select(); // In fact select the `input` so the user can retype the value if they change their mind.
    }

    render() {
        return <div>
            <input onChange={this.onValueChange} value={this.state.value} ref={this.onValueRef} />
            <select onSelect={this.onSuggestionSelect}>
                {this.state.suggestions.map(s => <option selected={this.state.value === s} value={s}>{s}</option>)}
            </select>
        </div>
    }
}
```

## Connected Components

To avoid have a single type for `props` and having to make `mapDispatchToProps` and `mapStateToProps` fields optional so they don't have to be specified in TSX, adopt this pattern instead:

```ts
// The state type.
type ComponentState = { isCollapsed: boolean; };

// The mapStateToProps type.
type ComponentStateProps = { strings: { [key: string]: string; /* … */ }; };

// The mapDispatchToProps type.
type ComponentDispatchProps = { showSignInPrompt: () => void; /* … */ };

// The JSX attributes type.
type ComponentOwnProps = { title: string; subtitle?: string; /* … */ };

type ComponentProps = ComponentStateProps & ComponentDispatchProps & ComponentOwnProps;

class Component extends React.PureComponent<ComponentProps, Partial<ComponentState>> {
  state: ComponentState = { isCollapsed: false; };
}

// Explicit return type so you are forced to return proper keys and only those.
function mapStateToProps(state: AppState): ComponentStateProps {
  return {
    strings: state.app.i18n.strings
  };
}

// Explicit return type so you are forced to return proper keys and only those.
function mapDispatchToProps(dispatch: any => any): ComponentDispatchProps {
  // Use bindActionCreators to not have to repeat the method signatures.
  return Redux.bindActionCreators({ showSignInPrompt });
}

export connect<ComponentStateProps, ComponentDispatchProps, ComponentOwnProps>(mapStateToProps, mapDispatchToProps)(Component);
```

Consider using the decorator syntax for `@connect`, but: https://github.com/TomasHubelbauer/react-redux-typescript-connect-decorator-demo

Keep the number of connected components small, strongly prefer passing data from parent component through `props`
before you decide to make a component connected.

## Connecting Page Entry Component

When connecting a Page Entry Component use Wrapper Class that will render the actual component.

Example:

```ts
//wrapper component
export default class ContinueWatching extends React.Component<ContinueWatchingOwnProps, any> {
  render() {
    return <ContinueWatchingConnected {...this.props} />;
  }
}
type ContinueWatchingOwnProps = ProgrammeCarouselProps & {}
type ContinueWatchingStateProps = {}
type ContinueWatchingProps = ContinueWatchingOwnProps & ContinueWatchingStateProps;

//actual component
class ContinueWatchingComp extends React.Component<ContinueWatchingProps, any> {
...
}
function mapStateToProps(state: state.Root): ContinueWatchingStateProps {...}
const ContinueWatchingConnected = connect<ContinueWatchingStateProps, any, ContinueWatchingOwnProps>(mapStateToProps, undefined)(ContinueWatchingComp);
```

## Initial State

Instead of this:

```ts
export class Component extends React.PureComponent<ComponentProps, Partial<ComponentState>> {
  constructor() {
    super();
    this.state = { isCollapsed: false };
  }
}
```

…consider using this:

```ts
export class Comp extends React.PureComponent<ComponentProps, Partial<ComponentState>> {
  // Explicit type so right hand side of the assignment is forced into proper shape.
  // Also `Partial` in generic parameter signature above not the type itself so in initialization it forces all fields to have default values.
  state: CompState = { isCollapsed: false };
}
```

Use proper `constructor` with a `props` argument when the initial state is dependent on `props` or invocation of an instance method.

## Binding action creators

Use `bindActionCreators` instead of object mapping with repeat action creator signatures in `mapDispatchToProps`.

Do:

```ts
function mapDispatchToProps(dispatch: any => any): CompDispatchProps {
  return Redux.bindActionCreators({ showSignInForm });

  // Alternatively when intending to adapt a signature of some action creators or renamed them:
  return {
    // Unchanged
    ...Redux.bindActionCreators({ unchanged1, unchanged2, showSignInForm }),
    // Changed
    changed: (newSignature: boolean) => original('constant', 1, newSignature ? 'yes' : 'no')
  }
}
```

Don't:

```ts
function mapDispatchToProps(dispatch: any => any): CompDispatchProps {
  // DO NOT DO THIS, SEE ABOVE
  return {
    doSomething: (a: string, b: number) => dispatch(doSomething(a, b)) // BAD - repeat signature with CompDispatchProps
  }
}
```

## Navigation

Avoid using `context.router`, instead use `import { browserHistory } from 'react-router'` and its methods:

- `goBack`
- `push`
- `replace`
- `pop`

## Imports

Use `import` for importing files instead of `require` for any resource type. Since ECMAScript 2015 `import` is standard JS.

 >`require` can be only used when importing npm module without typings, or dynamic imports, until upgrade to TypeScript 2.4 where you will be able to use `await import()`.

Instead of this:

```ts
require('./style.scss');
cosnt someComponent=require('someComponent').default;
```

…use this:

```ts
import './style.scss';
import someComponent from 'someComponent';
```

## `async`/`await` vs `then`, `catch`

Use `async`/`await`.

## Promises on unmounted components

Use `makeCancellable`, do not use deprecated `this.isMounted`, nor do use a class member for tracking mouting status.
